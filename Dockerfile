FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    apt-add-repository ppa:ansible/ansible-2.9 && \
    apt-get update && \
    apt-get install -y openssh-client git rsync sshpass ansible wget && \
    wget https://github.com/mitogen-hq/mitogen/archive/refs/tags/v0.2.9.tar.gz -O mitogen-0.2.9.tar.gz && \
    tar xvf mitogen-0.2.9.tar.gz && \
    apt-get purge -y software-properties-common && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /root/.ssh/ && chmod -R 600 /root/.ssh && \
    echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config

ENV ANSIBLE_STRATEGY_PLUGINS /mitogen-0.2.9/ansible_mitogen/plugins/strategy
ENV ANSIBLE_STRATEGY mitogen_linear
ENV ANSIBLE_HOST_KEY_CHECKING False

